const csv = require('csv-parser');
const fs = require('fs');
// const { endianness } = require('os');

const matches = [];
const delivery = [];

const matchesDetails = fs.createReadStream('../data/matches.csv');
const deliveries = fs.createReadStream('../data/deliveries.csv');

matchesDetails.pipe(csv())
.on('data', (data) => matches.push(data));


deliveries.pipe(csv())
.on('data', (data) => delivery.push(data));

const matchesPerYear = () => {
    matchesDetails.on('end', () => {
        let length = matches.length;
        ans = {};
        for (let i = 0; i < length; i++) {
            if (ans[matches[i]['season']] === undefined) 
            ans[matches[i]['season']] = 0;
            ans[matches[i]['season']] += 1;
        }
        fs.writeFile('../public/output/matchesPerYear.json', JSON.stringify(ans), 'utf-8', (err) => {
            if (err) return console.log(err);
        })
        // console.log(ans);
    })
}

const matchesWonPerTeamPerYear = (year) => {

    matchesDetails.on('end', () => {
        let length = matches.length;
        seasons = {};
        let cur = 0;
        ans = {};
        let itr = 0;
        for (let i = 0; i < length; ++i) {
            itr++;
            if (cur === 0) {
                cur = matches[i]['season'];
            } else if (cur != matches[i]['season']) {
                seasons[cur] = ans;
                cur = matches[i]['season'];
                ans = {}; 
            }
            if (matches[i]['season'] === cur) {
                    if (ans[matches[i]['winner']] === undefined) {
                        ans[matches[i]['winner']] = 0;
                    }
                    ans[matches[i]['winner']] += 1;
            }
        }
        seasons[cur] = ans;
        // return seasons;
        fs.writeFile('../public/output/matchesWonPerTeamPerYear.json', JSON.stringify(seasons), 'utf-8', (err) => {
            if (err) return console.log(err);
        })
        // console.log(seasons);
        
    })
}

const extraRunsConcededByTeam = () => {
    deliveries.on('end', () => {
        let length = delivery.length;
        ans = {};
        for (let i = 0; i < length; i++) {
            if (delivery[i]['match_id'] >= 577) {
                if (ans[delivery[i]['bowling_team']] === undefined) {
                    ans[delivery[i]['bowling_team']] = 0;
                }
                ans[delivery[i]['bowling_team']] += Number(delivery[i]['extra_runs']);
            }
        }
        // return ans;
        fs.writeFile('../public/output/extraRunsConcededByTeam.json', JSON.stringify(ans), 'utf-8', (err) => {
            if (err) return console.log(err);
        })
    })
}

const top10EconomicalBowlers = () => {

    deliveries.on('end', () => {
        ans = {};
        res = {};
        for (let i = 0; i < delivery.length; i++) {
            if (delivery[i]['match_id'] >= 518 && delivery[i]['match_id'] <= 576) {
                if (ans[delivery[i]['bowler']] === undefined) {
                    ans[delivery[i]['bowler']] = 0;
                }
                if (res[delivery[i]['bowler']] == undefined) {
                    res[delivery[i]['bowler']] = 0;
                }
                ans[delivery[i]['bowler']] += 1;
                res[delivery[i]['bowler']] += Number(delivery[i]['total_runs']);
            }
        }
        for (i in ans) {
            ans[i] = ans[i] / 6;
        }
        result = [];
        for (i in ans) {
            ans[i] = res[i] / ans[i];
            result.push([i, ans[i]]);
        }
        result.sort((a, b) => {
            return a[1] - b[1];
        })
        // result.sort();
        answer = {};
        for (let i = 0; i < 10; i++) {
            answer[result[i][0]] = result[i][1];
        }
        // console.log(answer);
        fs.writeFile('../public/output/top10EconomicBowlersIn2015.json', JSON.stringify(answer), 'utf-8', (err) => {
            if (err) return console.log(err);
        })
    })
}

// module.exports = matchesWonPerTeamPerYear;
module.exports = {matchesPerYear, matchesWonPerTeamPerYear, extraRunsConcededByTeam, top10EconomicalBowlers};



